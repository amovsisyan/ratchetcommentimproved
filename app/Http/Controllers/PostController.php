<?php

namespace App\Http\Controllers;

use App\Post;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class PostController extends Controller
{
    public function index()
    {
        return response()
            ->view('addPost');
    }

    public function addPost(Request $request)
    {
        /*validating*/
        $rules = [
            'postName'=>'required|min:3|max:30',
            'postContent'=>'required|min:3|max:1000',
            'file'=>'image|required',
        ];
        $this->validate($request, $rules);

        /*adding*/
        $file = $request->file('file');
        $filename = time().".".$file->getClientOriginalExtension();
        $created_by = Auth::id();
        $user = User::find($created_by);
        $new_post = new Post([
            'post_name'=>$request->postName,
            'post_content'=>$request->postContent,
            'post_avatar'=>$filename,
        ]);
        $user->posts()->save($new_post);
        Storage::disk('public')->put($filename,File::get($file));

        return redirect()->route('allposts_get');
    }

    public function currentPost($id)
    {
        $current_post = Post::where('post_id',$id)->first();
        $comments_for_this = $current_post->comments()
            ->leftJoin('users', function ($join) {
                $join->on('comments.created_by', '=', 'users.id');
            })
            ->select('users.name', 'comments.created_at', 'comments.comment_content', 'comments.comment_id as id')->orderBy('comments.created_at','desc')
            ->get();
        $auth =  User::where('id',Auth::id())->select('name','id')->first();
        $data = [];
        $data['current_post'] = $current_post;
        $data['comments'] = $comments_for_this;
        $data['auth'] = $auth;
        return response()
            ->view('currentPost', $data);
    }

    public function allPosts()
    {
        /*paginate all posts*/
        $per_page = 4;
        $all_posts = Post::paginate($per_page);

        return response()
            ->view('allPosts', ['all_posts' => $all_posts]);
    }

    public function changePost(Request $request)
    {
        try {
            /*validating*/
            $rules = [
                'post_name'=>'required|min:3|max:30',
                'post_content'=>'required|min:3|max:1000',
            ];
            $this->validate($request, $rules);

            /*changeing*/
            $post_name = $request['post_name'];
            $post_content = $request['post_content'];
            $id = $request['id_post'];

            /*if img send*/
            if($request->file('img')) {
                $img_name = Post::select('post_avatar')->where('post_id', '=', $id)->first();
                $img_path = $img_name->post_avatar;
                File::delete('img/posts/'.$img_path);
                $file = $request->file('img');
                $filename = time().".".$file->getClientOriginalExtension();
                Storage::disk('public')->put($filename,File::get($file));
            }

            /*changeing*/
            $user_id = Auth::id();
            $user = User::find($user_id);
            $user->posts()->where('post_id', $id)->update([
                'post_name'=>$post_name,
                'post_content'=>$post_content,
            ]);
            if (isset($filename)) {
                $user->posts()->where('post_id', $id)->update([
                    'post_avatar'=>$filename,
                ]);

                return response(json_encode(array('post_id'=>$id, 'post_name'=>$post_name, 'post_content'=>$post_content, 'post_avatar'=>$filename)));
            }

            return response(json_encode(array('post_id'=>$id, 'post_name'=>$post_name, 'post_content'=>$post_content)));
        }
        catch (\Exception $e) {
            return response(json_encode(array('error'=>$e->getMessage())));
        }
    }

    public function deletePost(Request $request)
    {
        try{
            $id = $request['id'];
            $img_name = Post::select('post_avatar')->where('post_id', $id)->first();
            $img_path = $img_name->post_avatar;

            File::delete('img/posts/'.$img_path);
            Post::where('post_id', $id)->delete();

            return response(json_encode(array('post_id'=>$id)));
        }
        catch (\Exception $e){
            return response(json_encode(array('error'=>$e->getMessage())));
        }
    }
}
