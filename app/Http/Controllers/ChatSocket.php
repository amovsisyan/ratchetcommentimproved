<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;
use Ratchet\ConnectionInterface;

class ChatSocket extends BaseSocket
{
    protected $clients;

    public  function __construct()
    {
        $this->clients = new \SplObjectStorage;
    }

    function onOpen(ConnectionInterface $conn){
        $this->clients->attach($conn);
        echo ("new connection {$conn->resourceId} \n");
    }

    function onClose(ConnectionInterface $conn)
    {
        $this->clients->detach($conn);
        echo ("connection {$conn->resourceId} disconnected \n");
    }

    function onError(ConnectionInterface $conn, \Exception $e)
    {
        echo ("error {$e->getMessage()} \n");
        $conn->close();
    }

    function onMessage(ConnectionInterface $from, $msg)
    {
        $runRec = count ($this->clients) - 1;

        $msg = json_decode($msg);

        if (isset($msg->isTyping)) {
            $result = [];
            $result['auth_name'] =  $msg->auth_name;
            $result['isTyping'] =  $msg->isTyping;
            $result['location'] =  $msg->location;
            $result['auth_id'] =  $msg->auth_id;
            $result_json = json_encode($result);
            foreach ($this->clients as $client){
                if($from !== $client){
                    $client->send($result_json);
                }
            }
        }

        if (isset($msg->newComm)) {
            $auth_id = $msg->auth_id;
            $comment = $msg->comment;
            $comment_id = $msg->comment_id;
            $data = new Comment([
                'created_by'=>$auth_id,
                'comment_content'=>$comment,
                'for_post' => $comment_id,
            ]);
            $data->save();
            $id = $data->id;
            $comment_append = Comment::where('comment_id',$id)->
            leftJoin('users', function ($join) {
                $join->on('comments.created_by', '=', 'users.id');
            })->select('users.name', 'users.id as u_id', 'comments.created_at', 'comments.comment_content', 'comments.comment_id as c_id')->first();
            $result = [];
            $result['createdAt'] = $comment_append->created_at;
            $result['name'] = $comment_append->name;
            $result['commentContent'] = $comment_append->comment_content;
            $result['u_id'] = $comment_append->u_id;
            $result['c_id'] = $comment_append->c_id;
            $result['location'] =  $msg->location;
            $result['newComm'] =  $msg->newComm;
            $result_json = json_encode($result);
            foreach ($this->clients as $client){
                if($from !== $client){
                    $client->send($result_json);
                }
            }
        }
    }
}
