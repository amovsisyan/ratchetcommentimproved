<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;


class BaseSocket implements MessageComponentInterface
{
    function onOpen(ConnectionInterface $conn)
    {

    }

    function onClose(ConnectionInterface $conn)
    {

    }

    function onError(ConnectionInterface $conn, \Exception $e)
    {

    }

    function onMessage(ConnectionInterface $from, $msg)
    {

    }
}
