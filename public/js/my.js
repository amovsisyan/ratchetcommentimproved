$(document).ready(function () {
    $(document).on("click",".post_edit",function() {
        var id = $(this).parents('.getter').attr('id');
        $post_name = $('#'+id).find("#post_name").html();
        $post_content = $('#'+id).find("#post_content").html();
        $('#pp_post_name').val($post_name);
        $('#pp_post_content').val($post_content);
        $('#pp_post_id').val(id);
        $('.pop_up').fadeIn();
    });

    $(document).on("click","#cancel",function() {
        $('.pop_up').fadeOut();
    });

    $(document).on("click","#save_change",function() {
        $pop_up = $('.pop_up');
        $button_save = $('#save_change');
        $button_canc = $('#cancel');
        $parent = $button_save.closest( "span" );
        $button_save.remove();
        $button_canc.remove();
        $('.pop_up .validation_error').remove();
        $parent.prepend('<img class="after_add" src="/img/forapp/load.gif">')
        var post_name = $('#pp_post_name').val();
        var post_content = $('#pp_post_content').val();
        var id_post = $('#pp_post_id').val();
        var file2 = document.getElementById("pp_file").files[0];

        var formData = new FormData();
        formData.append('post_name',post_name);
        formData.append('post_content',post_content);
        formData.append('id_post',id_post);
        formData.append('img',file2);
        $.ajax({
            type:'post',
            url:'/changepost',
            cache: false,
            enctype: 'multipart/form-data',
            data:formData,
            dataType: "json",
            processData: false,
            contentType: false,
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            success:function(data){
                if (data['error']) {
                    $('.pop_up .validation_error').remove();
                    $pop_up.append('<p class="validation_error"> The input is not valid </p>');
                    $('.after_add').remove();
                    $parent.append('<button id="save_change" class="btn btn-success">Save Changes</button>');
                    $parent.append('<button id="cancel" class="btn btn-default">Cancel</button>');
                    return;
                }
                var id = data['post_id'];
                $('#'+id).find('#post_name').html(data['post_name']);
                $('#'+id).find('#post_content').html(data['post_content']);
                if (data['post_avatar']) {
                    $('#'+data['post_id']).find('img').fadeOut('slow', function(){
                        var thisOne = $('#'+data['post_id']).find('img');
                        thisOne.attr('src','/img/posts/'+data['post_avatar'])
                        thisOne.fadeIn('slow');
                    })
                }
                $pop_up.fadeOut('slow', function() {
                    $('.after_add').remove();
                    $parent.append('<button id="save_change" class="btn btn-success">Save Changes</button>');
                    $parent.append('<button id="cancel" class="btn btn-default">Cancel</button>');
                });
            }
        })
    });

    $(document).on("click",".post_del",function() {
        $(this).parents('.getter').find('.post_edit').remove();
        $('.pop_up').fadeOut();
        var id = $(this).parents('.getter').attr('id');
        $(this).closest('div').html('<div class="btn btn-sm post_but"><img class="after_del" src="/img/forapp/load.gif"></div>');
        $.ajax({
            type:'delete',
            url:'/deletepost',
            dataType: "json",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            data:{
                id:id
            },
            success:function(data) {
                if (data.error) {
                    alert('The input is not valid'); /* Didn't want waste a time for this*/
                    return;
                }
                var dataId = data['post_id'];
                $('#'+dataId).fadeOut('slow', function() {
                    $('#'+dataId).remove();
                })
            }
        })
    });

    $(document).on('submit','form.adding',function(e){
        $button = $('#add_post');
        $parent = $button.closest( "div" );
        $button.remove();
        $parent.append('<div><img class="after_add" src="/img/forapp/load.gif"></div>')
    });

    /****************************************************************************************/
    /****************************************************************************************/
    /****************************************************************************************/

    /*****         important variables        ******/
    var all_info = [];
    var simply_info = {};
    var counter = 5;
    var com_scr_top = $('#comment').scrollTop() + $('#comment').height();
    var starting_ht = 0;

    /*****      this is event when user Click on Pop_up to see new Comments     ******/
    $(document).on("click",".new_com_info", function() {
        /**
         * I separate this two functions, cause when I press on pop_up I need both to scroll and to append
         * but when I add new comment but there is also comments that I havent read yet, I just need to append them, but not to scroll
         * */
        all_info.forEach(appendArrayElements);
        all_info.forEach(scrollArrayElements);
    });

    /*****         Appending not read comments             *****/
    function appendArrayElements(element, index, array) {
        var data = element.createdAt.date;
        var resData = data.slice(0, data.length-7);
        $('.needed').prepend('<div class="col-xs-12" id="c_'+element.c_id+'"> ' +
            '<div class="panel panel-white post panel-shadow"> ' +
            '<div class="post-heading"> <div class="pull-left meta"> ' +
            '<div class="title h5"> <a href="#"><b>' + element.name +'</b>' +
            '</a> </div>  <h6 class="text-muted time">made a post on. '+ resData +'</h6> ' +
            '</div>  </div> <div class="post-description">' +
            ' <p>'+ element.commentContent +'</p>          </div>              </div>          </div>')
        $('#comment').val('');
        $('.new_com_info').fadeOut('slow', function() {
            all_info.length = 0;
            simply_info = {};
            starting_ht = 0;
        });
    }

    /*****         Scrolling window till first added element             *****/
    function scrollArrayElements(element, index, array) {
        starting_ht += $('#c_'+element.c_id).height();
        mySetSctoll(starting_ht);
    }

    function mySetSctoll(starting_ht) {
        var top = com_scr_top + starting_ht;
        $(window).scrollTop(top);
    }

    /*****   start    SOCKET      *****/
    var conn  = new WebSocket ('ws://artcode.com:8080');

    /*****  sending Comment    SOCKET      *****/
    $('#comment').bind('keypress', function (e) {
        if (e.keyCode == 13) {
            $("#btn_send_comment").click();
            e.preventDefault()
        }
    });
    $(document).on("click","#btn_send_comment",function() {
        var comment = $('#comment').val();
        if (!comment) return;
        var location = window.location.pathname;
        var res = location.split("/");
        var auth_id = $('#auth_id').val();
        var auth_name = $('#auth_name').val();
        var data = {};
        data.auth_id = auth_id;
        data.comment = comment;
        data.comment_id = res[2];
        data.location = location;
        data.newComm = true;
        var sending = JSON.stringify(data);
        conn.send(sending);

        var dateObj = new Date();
        var year = dateObj.getFullYear();
        var month = dateObj.getMonth() + 1;
        var day = dateObj.getDate();
        var hour = dateObj.getHours();
        var min = dateObj.getMinutes();
        var sec = dateObj.getSeconds();
        var newdate = year + "-" + month + "-" + day + " " + hour + ":" + min + ":" + sec;
        all_info.forEach(appendArrayElements);
        $('.needed').prepend('<div class="col-xs-12"> ' +
            '<div class="panel panel-white post panel-shadow"> ' +
            '<div class="post-heading"> <div class="pull-left meta"> ' +
            '<div class="title h5"> <a href="#"><b>' + auth_name +'</b>' +
            '</a> </div>  <h6 class="text-muted time">made a post on. '+ newdate +'</h6> ' +
            '</div>  </div> <div class="post-description">' +
            ' <p>'+ comment +'</p>          </div>              </div>          </div>')
        $('#comment').val('');
    });

    /*****  send typing    SOCKET      *****/
    $('#comment').bind('keyup', function (e) {
        counter ++;
        if (counter > 5) {
            var comment = $('#comment').val();
            if (!comment) return;

            var auth_name = $('#auth_name').val();
            var auth_id = $('#auth_id').val();
            var data = {};
            var location = window.location.pathname;
            data.auth_name = auth_name;
            data.location = location;
            data.isTyping = true;
            data.auth_id = auth_id;

            var sending = JSON.stringify(data);
            conn.send(sending);
            counter = 0;
        }
    });

    /***** response  simple PopUp   SOCKET      *****/
    conn.onmessage = function (e) {
        var entity = JSON.parse(e.data);
        if (entity.location == window.location.pathname) {
            if (entity.newComm) {
                all_info.push(entity);
                var com_count = all_info.length;
                var u_id = all_info[com_count-1]['u_id'];
                if (simply_info[u_id]) {
                    simply_info[u_id]['count'] += 1;
                } else {
                    simply_info[u_id] = {};
                    simply_info[u_id]['name'] = all_info[com_count-1]['name'];
                    simply_info[u_id]['count'] = 1;
                }

                $('.new_com_info').html('<span>New Cooments -> ' + com_count + '<br/>Click to see them</span>');
                $.each(simply_info, function(index, value) {
                    $('.new_com_info').append('<div class="">'+value.count+' from '+value.name+'</div>');
                });
                $('#t_'+u_id).remove();
                $('.new_com_info').fadeIn();
            }

            /***** response  simple Typing   SOCKET      *****/
            if (entity.isTyping) {
                var id = entity.auth_id;
                if ($('#t_'+id).length != 0) return;
                $('#writingArea').find('.form-group').prepend('<div id=t_'+ id +' class="floatRight">'+ entity.auth_name + ' is typing   </div>');
                setTimeout(function() {
                    $('#t_'+id).remove();
                }, 5000);
            }
        }
    };
    /* end ****      SOCKET      *****/
});