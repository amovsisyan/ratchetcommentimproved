@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                @foreach($all_posts as $post)
                    <div class="col-xs-12 getter" id="{{$post->post_id}}">
                        <div class="outline">
                            @if (Auth::id()==$post->created_by)
                                <div>
                                    <button class="btn btn-danger btn-sm post_but post_del">X</button>
                                </div>
                                <div>
                                    <button class="btn btn-warning btn-sm post_but post_edit">ed</button>
                                </div>
                            @endif
                            <div class="col-xs-4 for_post_img">
                                <img src="/img/posts/{{ $post->post_avatar }}" alt="">
                            </div>
                            <div class="col-xs-8">
                                <div class="info">
                                    <p id="post_name">{{$post->post_name}}</p>
                                    <p id="post_content">{{$post->post_content}}</p>
                                </div>
                            </div>
                            <div class="see_more">
                                <a href="/allposts/{{ $post->post_id }}">
                                    <span>See More -></span>
                                </a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            {{ $all_posts->links() }}
        </div>
    </div>

    {{--        Editing Pop_Up      --}}
    <div class="col-xs-8 col-xs-offset-2 pop_up">
        <div class="form-group">
            <label>Your Post Name</label>
            <input type="text" class="form-control" id="pp_post_name" placeholder="Post Name" name="postName">
        </div>
        <div class="form-group">
            <label>Your Post Content</label>
            <input type="text" class="form-control" id="pp_post_content" placeholder="Post Content" name="postContent">
        </div>
        <div class="form-group">
            <label>Post IMG</label>
            <input type="file" class="form-control" id="pp_file" name="file">
        </div>
        <div class="form-group">
            <input id="pp_post_id" type="hidden" value="">
        </div>
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <span>
            <button id="save_change" class="btn btn-success">Save Changes</button>
            <button id="cancel" class="btn btn-default">Cancel</button>
        </span>
    </div>
@endsection
